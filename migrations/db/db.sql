CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS analytics (
  id uuid DEFAULT gen_random_uuid() NOT NULL PRIMARY KEY,
  time timestamptz DEFAULT now() NOT NULL,
  user_id uuid NOT NULL,
  data jsonb NOT NULL
)