package log

import (
	"fmt"
	"io"

	"github.com/rs/zerolog"
)

type Level int8

const (
	LDebug Level = iota
	LInfo
	LWarn
	LError
	LFatal
	LOff
	LTrace Level = -1
)

func init() {
	zerolog.TimeFieldFormat = "2006-01-02T15:04:05.000000Z07:00" // RFC3339 with microseconds
}

func NewLogger(handlers ...io.Writer) zerolog.Logger {
	return zerolog.New(zerolog.MultiLevelWriter(handlers...)).With().Timestamp().Logger()
}

type Handler struct {
	w     io.Writer
	level Level
}

func NewHandler(w io.Writer, level Level) *Handler {
	if w == nil {
		w = io.Discard
	}

	h := &Handler{
		w:     w,
		level: level,
	}

	return h
}

func (h *Handler) Write(p []byte) (int, error) {
	return h.w.Write(p)
}

func (h *Handler) WriteLevel(l zerolog.Level, p []byte) (n int, err error) {
	if zerologLevelToLevel(l) >= h.level {
		return h.w.Write(p)
	}

	return len(p), nil
}

var levelLabels = [...]string{"trace", "debug", "info", "warn", "error", "fatal", "off"}

func GetLevel(level string) (Level, error) {
	for i, v := range levelLabels {
		if level == v {
			return Level(i - 1), nil
		}
	}

	return LOff, fmt.Errorf("unknown log level %q", level)
}

func (l Level) String() string {
	if l < -1 || l > 5 {
		return "unknown"
	} else {
		return levelLabels[l+1]
	}
}

func zerologLevelToLevel(l zerolog.Level) Level {
	switch l {
	case zerolog.TraceLevel, zerolog.DebugLevel, zerolog.InfoLevel, zerolog.WarnLevel, zerolog.ErrorLevel:
		return Level(l)
	case zerolog.FatalLevel:
		return LFatal
	default:
		return LOff
	}
}
