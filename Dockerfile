FROM golang:1.21.0-alpine3.18 AS builder

RUN apk update --no-cache && \
  apk add --no-cache tzdata

ENV CGO_ENABLED=0
ENV GOOS=linux

WORKDIR /app/audit

ADD go.mod .
ADD go.sum .
RUN go mod download

COPY . .

RUN go build \
  -ldflags="-s -w \
  -X 'main.AppName=audit' \
  -X 'main.BuildDate=$(date)'" \
  -o ./bin/audit ./cmd/audit/main.go

FROM alpine:3.18

COPY --from=builder /usr/share/zoneinfo/Europe/Moscow /usr/share/zoneinfo/Europe/Moscow
ENV TZ=Europe/Moscow

WORKDIR /app/bin

COPY --from=builder /app/audit/bin .

ENTRYPOINT [ "./audit" ]