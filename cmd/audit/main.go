package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"audit/internal/app"
	"audit/internal/config"
	"audit/pkg/log"
)

var (
	AppName   = "n/a"
	BuildDate = "n/a"
)

const GracefulShutdownTimeout = 60

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	config := config.LoadConfig()

	logLevel, err := log.GetLevel(config.LogLevel)
	if err != nil {
		panic(fmt.Sprintf("setting logger error: %v", err))
	}

	log := log.NewLogger(log.NewHandler(os.Stdout, logLevel))

	app.Log = log.With().Bool("app", true).Logger()
	app.Log.Info().Bool("app", true).Str("state", "start").Strs("app_info", []string{AppName, BuildDate}).Send()
	defer app.Log.Info().Bool("app", true).Str("state", "stop").Send()

	defer app.PanicHandler(log, cancel)

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		cause := (<-sig).String()

		app.Log.Info().Str("state", "stopping").Str("cause", cause).Send()
		cancel()

		<-time.After(time.Duration(GracefulShutdownTimeout) * time.Second)

		app.Log.Warn().Str("state", "stop").Bool("force", true).Send()
		os.Exit(1)
	}()

	app.Run(ctx, config, log)
}
