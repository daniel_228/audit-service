# Сервис сборки аналитики (аудита)

## Запуск

```shell
docker compose up -d
```

## Конфигурация

[.env](.env) - параметры docker-контейнеров

- DB_TAG - тег докер-образа БД
- DB_HOST - внутреннее имя хоста БД
- DB_PORT - порт БД
- DB_USER - пользователь БД
- DB_PASSWORD - пароль пользователя БД
- DB_NAME - имя базы данных
- AUDIT_PORT - порт, который прослушивает сервис аудита

[config/service.env](config/service.env) - параметры сервиса аудита

- LOG_LEVEL - уровень логирования (допустимые значения: trace, debug, info, warn, error, fatal, off)
- WORKER_CAP - ёмкость канала воркер пула и количество воркеров

## Эндопоинты

### POST /analytics

```http
POST http://localhost:8080/analytics HTTP/1.1
X-Tantum-UserAgent: DeviceID=G1752G75-7C56-4G49-BGFA-5ACBGC963471;DeviceType=iOS;OsVersion=15.5;AppVersion=4.3 (725)
X-Tantum-Authorization: 2daba111-1e48-4ba1-8753-2daba1119a09
Content-Type: application/json

{
  "module": "settings",
  "type": "alert",
  "event": "click",
  "name": "подтверждение выхода",
  "data": {
    "action": "cancel"
  }
}
```
