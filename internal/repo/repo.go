package repo

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog"

	"audit/internal/config"
	"audit/internal/models/entities"
)

var Name = "repo"

type Repo interface {
	AddEntry(ent *entities.Analytics) error
}

const (
	ConnRetries = 20
)

type repo struct {
	config *config.Config
	log    zerolog.Logger
	db     *sqlx.DB
}

func NewRepo(config *config.Config, log zerolog.Logger) *repo {
	return &repo{
		config: config,
		log:    log,
	}
}

func (r *repo) Start(ctx context.Context) error {
	period := 16 * time.Millisecond

	ticker := time.NewTicker(period)
	defer ticker.Stop()

	var err error

	for i := 1; i <= ConnRetries; i++ {
		select {
		case <-ctx.Done():
			return err
		case <-ticker.C:
		}

		r.log.Trace().Msgf("Attempt %d: connecting to DB", i)

		err = r.connect()
		if err != nil {
			r.log.Trace().Msgf("Attempt %d failed: DB error: %v", i, err)

			if period < 5*time.Second {
				period *= 2
			} else {
				period = 10 * time.Second
			}

			ticker.Reset(period)
		} else {
			r.log.Trace().Msgf("Successful DB connection")
			return nil
		}
	}

	r.log.Error().Err(err).Msg("Can't connect to DB")
	return err
}

func (r *repo) connect() error {
	db, err := sqlx.Connect("postgres", r.config.DBUrl)
	if err != nil {
		return err
	}

	r.db = db
	return nil
}

func (r *repo) Stop(ctx context.Context) error {
	if r.db != nil {
		return r.db.Close()
	}

	return nil
}
