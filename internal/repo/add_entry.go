package repo

import (
	"audit/internal/models/entities"
)

func (r *repo) AddEntry(ent *entities.Analytics) error {
	query := `INSERT INTO analytics(user_id, data) VALUES(:user_id, :data)`

	_, err := r.db.NamedExec(query, ent)

	return err
}
