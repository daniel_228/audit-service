package config

import (
	"os"
	"strconv"
)

type Config struct {
	LogLevel  string
	Port      string
	WorkerCap int
	DBUrl     string
}

func LoadConfig() *Config {
	workerCap, err := strconv.Atoi(os.Getenv("WORKER_CAP"))
	if err != nil {
		workerCap = 10 // default value
	}

	return &Config{
		LogLevel:  os.Getenv("LOG_LEVEL"),
		Port:      os.Getenv("PORT"),
		WorkerCap: workerCap,
		DBUrl:     os.Getenv("DB_URL"),
	}
}
