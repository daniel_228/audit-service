package http_handler

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"

	"audit/internal/config"
	"audit/internal/service"
)

var Name = "http handler"

type HTTPHandler interface {
	Analytics(w http.ResponseWriter, r *http.Request)
}

type httpHandler struct {
	config  *config.Config
	log     zerolog.Logger
	service service.Service
	server  *http.Server
}

func NewHTTPHandler(config *config.Config, log zerolog.Logger, service service.Service) *httpHandler {
	return &httpHandler{
		config:  config,
		log:     log,
		service: service,
	}
}

func (h *httpHandler) Start(ctx context.Context) error {
	router := mux.NewRouter()
	router.StrictSlash(true)

	router.HandleFunc("/analytics", h.Analytics).Methods("POST")

	h.server = &http.Server{
		Addr:    ":" + h.config.Port,
		Handler: router,
	}

	go func() {
		if err := h.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			h.log.Error().Err(err).Msg("ListenAndServe() error")
		}
	}()

	return nil
}

func (h *httpHandler) Stop(ctx context.Context) error {
	return h.server.Shutdown(ctx)
}

func (h *httpHandler) httpRespondWithError(w http.ResponseWriter, code int, message string) {
	h.httpRespondWithJSON(w, code, map[string]string{"error": message})
}

func (h *httpHandler) httpRespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		h.log.Err(err).Msg("json marshall error")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
