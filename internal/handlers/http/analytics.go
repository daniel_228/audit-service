package http_handler

import (
	"encoding/json"
	"net/http"

	"github.com/google/uuid"

	"audit/internal/models/dto"
)

func (h *httpHandler) Analytics(w http.ResponseWriter, r *http.Request) {
	entry := &dto.Entry{}

	var err error

	entry.UserID, err = uuid.Parse(r.Header.Get("X-Tantum-Authorization"))
	if err != nil {
		h.httpRespondWithError(w, http.StatusForbidden, "Invalid X-Tantum-Authorization")
		return
	}

	entry.Headers = r.Header

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&entry.Body); err != nil {
		h.httpRespondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}

	r.Body.Close()

	if err := h.service.AddEntry(entry); err != nil {
		h.httpRespondWithJSON(w, http.StatusInternalServerError, map[string]string{"error": "unknown"})
		return
	}

	h.httpRespondWithJSON(w, http.StatusAccepted, map[string]string{"status": "OK"})
}
