package entities

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	"github.com/google/uuid"
)

type Analytics struct {
	ID     uuid.UUID `db:"id"`
	Time   time.Time `db:"time"`
	UserID uuid.UUID `db:"user_id"`
	Data   Data      `db:"data"`
}

type Data map[string]any

func (d *Data) Scan(val any) error {
	b, ok := val.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &d)
}
func (d Data) Value() (driver.Value, error) {
	return json.Marshal(d)
}
