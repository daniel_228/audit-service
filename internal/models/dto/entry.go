package dto

import (
	"encoding/json"
	"net/http"

	"github.com/google/uuid"
)

type Entry struct {
	UserID  uuid.UUID
	Headers http.Header
	Body    json.RawMessage
}
