package app

import (
	"context"
	"runtime/debug"

	"github.com/rs/zerolog"

	"audit/internal/config"
	http_handler "audit/internal/handlers/http"
	"audit/internal/repo"
	"audit/internal/service"
)

type Control interface {
	Start(context.Context) error
	Stop(context.Context) error
}

type component[T any] struct {
	Name string
	Control
	Impl T
}

func NewComponent[T any](name string, impl T) component[T] {
	return component[T]{name, any(impl).(Control), impl}
}

type application struct {
	Repo        component[repo.Repo]
	Service     component[service.Service]
	HTTPHandler component[http_handler.HTTPHandler]
}

var App *application
var Log zerolog.Logger

func init() {
	App = &application{}
}

func Run(ctx context.Context, config *config.Config, log zerolog.Logger) {
	// repo
	App.Repo = NewComponent[repo.Repo](repo.Name, repo.NewRepo(config, componentLogger(log, repo.Name)))

	if ctx.Err() != nil || !start(ctx, App.Repo) {
		return
	}
	defer stop(ctx, App.Repo)

	// service
	App.Service = NewComponent[service.Service](service.Name, service.NewService(config, componentLogger(log, service.Name), App.Repo.Impl))

	if ctx.Err() != nil || !start(ctx, App.Service) {
		return
	}
	defer stop(ctx, App.Service)

	// http_handler
	App.HTTPHandler = NewComponent[http_handler.HTTPHandler](http_handler.Name, http_handler.NewHTTPHandler(config, componentLogger(log, http_handler.Name), App.Service.Impl))

	if ctx.Err() != nil || !start(ctx, App.HTTPHandler) {
		return
	}
	defer stop(ctx, App.HTTPHandler)

	<-ctx.Done()
}

func PanicHandler(log zerolog.Logger, handle func()) {
	if r := recover(); r != nil {
		log.Fatal().Msgf("panic: %v\n\n%s", r, string(debug.Stack()))

		handle()
	}
}

func start[T any](ctx context.Context, c component[T]) bool {
	if err := c.Start(ctx); err != nil {
		Log.Error().Str("state", "start").Str("component", c.Name).Err(err).Send()
		return false
	}

	Log.Info().Str("state", "start").Str("component", c.Name).Send()
	return true
}

func stop[T any](ctx context.Context, c component[T]) {
	if err := c.Stop(ctx); err != nil {
		Log.Error().Str("state", "stop").Str("component", c.Name).Err(err).Send()
	} else {
		Log.Info().Str("state", "stop").Str("component", c.Name).Send()
	}
}

func componentLogger(log zerolog.Logger, name string) zerolog.Logger {
	return log.With().Str("component", name).Logger()
}
