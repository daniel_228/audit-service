package service

import (
	"runtime/debug"

	"audit/internal/models/dto"
)

func (s *service) AddEntry(entry *dto.Entry) error {
	go func() {
		defer func() {
			if r := recover(); r != nil {
				s.log.Error().Msgf("panic: %v\n\n%s", r, string(debug.Stack()))
			}
		}()

		select {
		case s.jobs <- entry:
		case <-s.done:
		}
	}()

	return nil
}
