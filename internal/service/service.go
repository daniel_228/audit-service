package service

import (
	"context"
	"runtime/debug"
	"sync"

	"github.com/rs/zerolog"

	"audit/internal/config"
	"audit/internal/models/dto"
	"audit/internal/models/entities"
	"audit/internal/repo"
)

var Name = "service"

type Service interface {
	AddEntry(entry *dto.Entry) error
}

type service struct {
	config *config.Config
	log    zerolog.Logger
	repo   repo.Repo
	jobs   chan *dto.Entry
	done   chan struct{}
	wg     sync.WaitGroup
}

func NewService(config *config.Config, log zerolog.Logger, repo repo.Repo) *service {
	return &service{
		config: config,
		log:    log,
		repo:   repo,
		jobs:   make(chan *dto.Entry, config.WorkerCap),
		done:   make(chan struct{}),
	}
}

func (s *service) Start(ctx context.Context) error {
	s.wg.Add(s.config.WorkerCap)

	for i := 0; i < s.config.WorkerCap; i++ {
		i := i

		go func() {
			defer s.wg.Done()
			defer func() {
				if r := recover(); r != nil {
					s.log.Error().Msgf("panic: %v\n\n%s", r, string(debug.Stack()))
				}
			}()

			s.worker(i)
		}()
	}

	return nil
}

func (s *service) worker(num int) {
	s.log.Debug().Int("worker", num).Msg("start worker")

	for {
		select {
		case <-s.done:
			s.log.Debug().Int("worker", num).Msg("stop worker")
			return
		case entry, ok := <-s.jobs:
			if !ok {
				s.log.Debug().Int("worker", num).Msg("stop worker because jobs chan has closed")
				return
			}

			if err := s.repo.AddEntry(&entities.Analytics{
				UserID: entry.UserID,
				Data: entities.Data{
					"headers": entry.Headers,
					"body":    entry.Body,
				},
			}); err != nil {
				s.log.Error().Int("worker", num).Err(err).Msg("add entry db error")
			} else {
				s.log.Debug().Int("worker", num).Err(err).Msg("successful add entry db")
			}
		}
	}
}

func (s *service) Stop(ctx context.Context) error {
	close(s.done)
	s.wg.Wait()
	return nil
}
